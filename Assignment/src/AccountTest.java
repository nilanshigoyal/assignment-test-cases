import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {
	@Test
	public void testAllAccountsAreInitializedWithZeroBalance() {
		Account ac = new Account(100);
		int actualBalance = ac.balance();
		
		assertEquals(0, actualBalance);
	}
	
	@Test
	public void testMoneyCanBeWithdrawn() {
		Account ac = new Account(100);
		int balanceBefore = ac.balance();
		ac.withdraw(10);
		int balanceAfter = ac.balance();
		assertEquals(10, balanceBefore - balanceAfter);
	}
	
	@Test
	public void testMoneyCanBeDeposited() {
		Account ac = new Account(100);
		int balanceBfr = ac.balance();
		ac.deposit(200);
		int balanceAfter= ac.balance();
		assertEquals(balanceBfr+200,balanceAfter);
	}
	@Test
	public void testAllAccountsShouldHaveSufficientBalanceForWithdraw() {
		Account ac = new Account(100);
		int actualBalance = ac.balance();
		ac.withdraw(actualBalance+10);
		int balanceAfterWithdrawl= ac.balance();
		assertEquals(balanceAfterWithdrawl,actualBalance);
	}
	@Test
	public void testAccountsShouldBeUpdatedAsTransactionPerformed() {
		Account ac = new Account(100);
		int actualBalance = ac.balance();
		ac.withdraw(10);
		int balanceAfterWithdrawl= ac.balance();
		assertEquals(90, balanceAfterWithdrawl);
	}
	@Test
	public void testDepositOccursImmediately() {
		Account ac = new Account(100);
		int actualBalance = ac.balance();
		ac.deposit(10);
		int balanceAfterDeposit= ac.balance();
		assertEquals(110,balanceAfterDeposit);
	}
}
