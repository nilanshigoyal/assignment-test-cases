import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void testEveryCustomerHasAnAccount() {
		Customer cu = new Customer("Nilanshi", 100);
		Account ac = cu.getAccount();
		assertNotNull(ac);
	}

	@Test
	public void testCustomerHasChoice() {
		Customer cu = new Customer("Nilanshi", 100);
		Account ac = cu.getAccount();
		int balanceAfterOpening = ac.balance();
		assertEquals(100, balanceAfterOpening);
	}
	@Test
	public void testCustomerHasChoiceToInitializeWithZero() {
		Customer cu = new Customer("Nilanshi", 0);
		Account ac = cu.getAccount();
		int balanceAfterOpening = ac.balance();
		assertEquals(0, balanceAfterOpening);
	}
}
